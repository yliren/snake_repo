function ret = pre_cell(states, trans_set, act)
	% PRE: computes the 1-step Pre set of a set of states under the action(s) act
	% if no action is specified, the Pre set of all action is computed
	if nargin<3
		act = 1:length(trans_set);
	end
	ret = [];
	for a = act
		ret = union(ret, find(sum(trans_set{a}(:,states),2)>=1));
	end
	ret = reshape(ret, 1, length(ret));
end