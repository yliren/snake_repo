function pre_set = pre_action_forall_cell(X, trans_set, act)
	% Computes the set pre_set, such that for all s \in pre_set,
	% s -> X under action act.
	N_states = size(trans_set{1},1);
	pre_cand = pre_cell(X, trans_set, act);
	bad_states = setdiff(post_cell(pre_cand, trans_set, act), X);
	pre_set = pre_cand(find(sum(trans_set{act}(pre_cand,bad_states),2) == 0));
	pre_set = reshape(pre_set, 1, length(pre_set));
end