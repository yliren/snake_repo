clear,clc

addpath('lib_abstr')
addpath('lib_cinv_synthesis')
addpath('lib_plot')
addpath('sys1')

sys = load_sys1();

X_safe = label_sys(sys); 

[C,K] = findControlledInvariantMulti_cell(X_safe, sys.tau);

